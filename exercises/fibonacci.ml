(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-33-34-37-39"]

open Fold
open Nat

(*****************************************************************)
(* Algebra                                                       *)
(*****************************************************************)

module AlgFib = struct
  module F = CNat

  type carrier1 = NYI

  type carrier2 = NYI

  let alg1 _fib _fib' = failwith "NYI"

  let alg2 _fib _fib' = failwith "NYI"
end

(*****************************************************************)
(* Fixpoint                                                      *)
(*****************************************************************)

let fib _ = failwith "NYI"

(*****************************************************************)
(* Tests                                                         *)
(*****************************************************************)

let%test _ =
  let x = Spec.Zero in
  Spec.fib x = fib (from_nat x)

let%test _ =
  let x = Spec.Succ Spec.Zero in
  Spec.fib x = fib (from_nat x)

let%test _ =
  let x = Spec.Succ (Spec.Succ Spec.Zero) in
  Spec.fib x = fib (from_nat x)

let%test _ =
  let x = Spec.Succ (Spec.Succ (Spec.Succ Spec.Zero)) in
  Spec.fib x = fib (from_nat x)
